<?php

/**
 * Check environment is local.
 *
 * @return bool
 */
function ic_is_local(): bool {
	return wp_get_environment_type() === 'local';
}

/**
 * Check environment is development.
 *
 * @return bool
 */
function ic_is_development(): bool {
	return wp_get_environment_type() === 'development';
}

/**
 * Check environment is staging.
 *
 * @return bool
 */
function ic_is_staging(): bool {
	return wp_get_environment_type() === 'staging';
}

/**
 * Check environment is production
 *
 * @return bool
 */
function ic_is_production(): bool {
	return wp_get_environment_type() === 'production';
}
